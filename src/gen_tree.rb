def gen_tree(depth)
  index = 0
  depth -= 1

  root = Node.new(index, rand(0..100))
  parents = [root]

  while (depth > 0)
    new_parents = []

    parents.each do |parent|
      2.times do |child|
        index += 1
        node = Node.new(index, rand(0..100))
        node.parent = parent
        new_parents << node
      end
    end

    parents = new_parents
    depth -= 1;
  end

  return root
end
