class Node
  attr_accessor :value
  attr_reader :parent
  attr_reader :children
  attr_reader :id

  def initialize(id, value)
    @id = id
    @value = value
    @children = []
  end

  def [](index)
    @children[index]
  end

  def parent=(new_parent)
    return if new_parent == @parent

    @parent.remove_child(self) unless @parent.nil?
    @parent = new_parent
    new_parent.children << self
  end

  def remove_child(child)
    @children = @children
      .reject{ |node| node == child }
  end

  def inspect
    result = "NODE: #{@value}"
    #result += ", children: #{children}" if @children.size > 0
    "<#{result}>"
  end
end
