require 'optparse'

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: example.rb [options]"

  opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
    options[:verbose] = v
  end
end.parse!

p options
p ARGV

return

require 'pry'
require 'graphviz'

require_relative 'node'
require_relative 'gen_tree'


def each_node(root, &block)
  return root unless block
  children = root.children
  block.(root)

  children.each do |child|
    each_node(child, &block)
  end
end

def enum_each(root)
  Enumerator.new do |y|
    each_node(root) do |node|
      y << node
    end

    true
  end
end

root = gen_tree(9)

g = GraphViz.new( :G, :type => :digraph )
enum_each(root).each do |node|
  g.add_nodes(node.id.to_s, label: node.id)
  g.add_edges(node.parent.id.to_s, node.id.to_s) if node.parent
end

g.output(png: 'graph.png')


